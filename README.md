Steps given below

Make sure the machine is connected to internet.

1. Under the root dir create a .env file with following data

MONGO_USERNAME=your_mongodb_username
MONGO_PASSWORD=your_mongodb_password
REDIS_PASSWORD=your_redis_password

2. Create a python virtualenv, activate it and install the dependencies using command - 
pip install -r requirements.txt

3. Run the file initialize.py

4. After initilize.py running is completed run the server 
   using python app.py command.

5. Open your browser and hit 'http://127.0.0.1:5000'

