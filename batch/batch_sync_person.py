# Core import

# Third party Import
import pandas as pd
from pymongo import errors

# App level import
from config import settings


class BatchSyncPerson:
    """
    Class to sync the data from the specified url into mongo.
    The instance have only one attribute i,e url.
    The attribute takes the assignment specified url as the
    defult one.
    """

    def __init__(self, url=settings.SYNC_URL):
        self.url = url

    def perform_sync(self):
        """
        Insert the data into mongo after fetching from the url.
        Also logs the inserted data into a file.
        """
        sync_status = False
        dataset = self.get_formatted_dataset()

        try:
            # If some docs exits remove them.
            # We dont want any duplicates here.
            settings.Person.delete_many({})
            settings.Person.insert_many(dataset)
            sync_status = True
        except (TypeError, errors.ServerSelectionTimeoutError,
                errors.OperationFailure) as e:
            print('Oops somthing went wrong: {}'.format(e))

        return sync_status

    def get_formatted_dataset(self):
        """
        Read the data from the http url path.
        Return a list which is inserted in bulk into mongo
        """
        person_list = []

        try:
            df = pd.read_csv(
                self.url, header=None,
                names=settings.PERSON_ATTRIBUTES, index_col=False)

        except (HTTPError, ParserError):
            return []

        for index, row in df.iterrows():
            cleaned_data = {}

            for field, value in dict(row).items():
                if isinstance(value, str):
                    value = value.strip().lower()
                cleaned_data[field] = value

            person_list.append(cleaned_data)

        return person_list

    def __call__(self):
        if self.perform_sync():
            return True
        else:
            return False
