# Core import
import sys

# Third party import
from pymongo import (
    errors, ASCENDING
)

# App level import
from config import settings
from batch import batch_sync_person

WAIT_MESSAGE = 'Hold on data is getting populated'
SUCCESS_MESSAGE = 'Data populated successfully'
ERROR_MESSAGE = 'Could not populate data.'


def create_index():
    """
    Did not used compound index here.
    This is because filter query can use one or more fields among three
    which might not use the compund index.
    """
    try:
        settings.Person.create_index([('sex', ASCENDING)])
        settings.Person.create_index([('relationship', ASCENDING)])
        settings.Person.create_index([('race', ASCENDING)])

    except (errors.ServerSelectionTimeoutError, errors.OperationFailure) as e:
        print('Mongo Error. Details: {}'.format(e))
        sys.exit(1)

if __name__ == '__main__':
    create_index()

    print(WAIT_MESSAGE)

    if batch_sync_person.BatchSyncPerson()():
        print(SUCCESS_MESSAGE)
    else:
        print(ERROR_MESSAGE)
