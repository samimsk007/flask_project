# Third party import
from flask import (
    Flask, jsonify, request, render_template
)

# App level import
from config import cache
from controller import (
    gender_statistics, relationship_statistics, filter_person,
    filter_result
)

app = Flask(__name__)


@app.route('/', methods=['GET'])
# @cache.cache
def dashboard():
    return render_template('dashboard.html')


@app.route('/sex-distribution', methods=['GET'])
@cache.cache
def gender_statistics_api():
    result = gender_statistics.get_gender_data()
    return jsonify(result)


@app.route('/relationship-statistics', methods=['GET'])
@cache.cache
def relationship_statistics_api():
    result = relationship_statistics.get_relationship_data()
    return jsonify(result)


@app.route('/filter-person', methods=['GET'])
@cache.cache
def filter_person_api():
    result = filter_person.filter(request)
    return jsonify(result)


@app.route('/filter-result', methods=['GET'])
@cache.cache
def filter_result_api():
    result = filter_result.filter()
    return jsonify(result)


if __name__ == '__main__':
    app.run()
