# Core import
import os

# Third party import
import redis
import pymongo
from dotenv import load_dotenv

# Load the .env vars
ROOT_PATH = os.path.abspath('.')
ENV = os.path.join(ROOT_PATH, '.env')
load_dotenv(ENV)

# Get mongo credentials from loaded env vars
MONGO_HOST = os.getenv('MONGO_HOST', 'localhost')
MONGO_PORT = os.getenv('MONGO_PORT', '27017')
MONGO_USERNAME = os.getenv('MONGO_USERNAME')
MONGO_PASSWORD = os.getenv('MONGO_PASSWORD')
MONGO_AUTH_SOURCE = os.getenv('MONGO_AUTH_SOURCE', 'admin')

# Initilize mongo connection params
CONNECTION_STRING = ('mongodb://{username}:{password}@{host}:{port}/'
                     '?authSource={authsource}'.format(
                         username=MONGO_USERNAME, password=MONGO_PASSWORD,
                         host=MONGO_HOST, port=MONGO_PORT,
                         authsource=MONGO_AUTH_SOURCE))

# Connect mongo and initialize which database to use
mongo_connection = pymongo.MongoClient(CONNECTION_STRING)
db = mongo_connection.adult_dataset

# Define which collection to use
Person = db.person

# Data fetch url
SYNC_URL = ('http://archive.ics.uci.edu/ml/machine-learning-databases'
            '/adult/adult.data')

# adult_dataset column names
PERSON_ATTRIBUTES = ['age', 'workclass', 'fnlwgt', 'education',
                     'num', 'status', 'occupation', 'relationship',
                     'race', 'sex', 'gain', 'loss', 'week', 'country']

# Redis credential
REDIS_HOST = os.getenv('REDIS_HOST', 'localhost')
REDIS_PORT = os.getenv('REDIS_PORT', 6379)
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD')
REDIS_DB = 5

# Connect to redis client
CONNECTION_POOL = redis.ConnectionPool(
    host=REDIS_HOST, port=REDIS_PORT, db=5, password=REDIS_PASSWORD)
CACHE_REDIS_CLIENT = redis.StrictRedis(
    connection_pool=CONNECTION_POOL)

# Timeout
CACHE_TIME_OUT = 3600
