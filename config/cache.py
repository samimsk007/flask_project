# Core import
import pickle
from functools import wraps

# App level import
from flask import request

# App level import
from config import settings


def cache(func):

    @wraps(func)
    def decorated_function(*args, **kwargs):
        url = request.url

        try:
            response = settings.CACHE_REDIS_CLIENT.get(url)
            if not response:
                response = func()
                response_to_cache = pickle.dumps(response)
                settings.CACHE_REDIS_CLIENT.set(
                    url, response_to_cache, ex=settings.CACHE_TIME_OUT)
            else:
                response = pickle.loads(response)
        except:
            response = func()
        return response

    return decorated_function
