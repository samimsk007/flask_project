# Third party import
from pymongo import errors

# App level import
from config import settings


def filter(request):
    result, mongo_query = [], {}
    query_params = request.args

    for query, value in query_params.items():
        mongo_query[query] = value

    skip, limit = paginate(request)

    if 'page_size' in mongo_query:
        mongo_query.pop('page_size')
    if 'page_number' in mongo_query:
        mongo_query.pop('page_number')

    try:
        result = settings.Person.find(
            mongo_query, {'_id': 0}).skip(skip).limit(limit)
    except (errors.ServerSelectionTimeoutError, errors.OperationFailure):
        pass

    result = list(result)
    return result


def paginate(request):
    try:
        page_number = request.args.get('page_number', 1)
        page_number = int(page_number)
    except ValueError:
        page_number = 0

    try:
        page_size = request.args.get('page_size', 20)
        page_size = int(page_size)
    except ValueError:
        page_size = 20

    # Mongo Skip cannot take input less than 0.
    if page_number <= 0:
        page_number = 1

    skip = (page_number - 1) * page_size
    limit = page_size

    return skip, limit
