# Core level import
from collections import defaultdict

# Third party import
from pymongo import errors

# App level Import
from config import settings


def get_gender_data():
    result = defaultdict(int)
    sex = ['male', 'female']

    try:
        for each_sex in sex:
            total = settings.Person.count({'sex': each_sex})
            result[each_sex] = total
    except (errors.ServerSelectionTimeoutError, errors.OperationFailure):
        pass

    result = [{gender: count} for gender, count in result.items()]
    return result
