# Third party import
from pymongo import errors

# App level import
from config import settings


def filter():
    result = {}
    result['sex'] = set()
    result['relationship'] = set()
    result['race'] = set()

    try:
        persons = settings.Person.find({})

        for each_person in persons:
            sex = each_person.get('sex')
            relationship = each_person.get('relationship')
            race = each_person.get('race')

            result['sex'].add(sex)
            result['relationship'].add(relationship)
            result['race'].add(race)

    except (errors.ServerSelectionTimeoutError, errors.OperationFailure):
        pass

    for filter_type, data in result.items():
        result[filter_type] = list(data)

    return result
