# Core level import
from collections import defaultdict

# Third party import
from pymongo import errors

# App level import
from config import settings


def get_relationship_data():
    """
    With proper indexing this would be faster than below
    commented out function.
    """
    result = defaultdict(int)
    relationship_types = ["not-in-family", "husband", "wife", "own-child",
                          "unmarried", "other-relative"]

    try:
        for each_relation in relationship_types:
            total = settings.Person.count({'relationship': each_relation})
            result[each_relation] = total
    except (errors.ServerSelectionTimeoutError, errors.OperationFailure):
        pass

    result = [{relation: count} for relation, count in result.items()]
    return result


# Do this way - i,e single db call
# then computing each relation in python is taking
# more time. So did not implemented this.

# def get_relationship_data():
#     result = defaultdict(int)

#     try:
#         person_list = settings.Person.find()

#         for person in person_list:
#             relationship = person.get('relationship')
#             result[relationship] += 1

#     except (ServerSelectionTimeoutError, OperationFailure):
#         pass

#     result = [{relation: count} for relation, count in result.items()]
#     return result
