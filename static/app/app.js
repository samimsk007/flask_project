var protocol = 'http://'
var hostname = protocol + window.location.host
var application = angular.module('pocapp', [])

application.config(function($interpolateProvider, $httpProvider){         
        $interpolateProvider.startSymbol('[[').endSymbol(']]');
    });


var graph1_colors = ['rgba(75, 192, 192, 0.2)',
                     'rgba(153, 102, 255, 0.2)'
                    ]

var graph2_colors = ['rgba(255, 99, 132, 0.2)',
                     'rgba(54, 162, 235, 0.2)',
                     'rgba(255, 206, 86, 0.2)'
                    ]

application.controller('genderChart', function($scope, $http){
    var url = hostname + '/sex-distribution';

    $http.get(url)
    .then(function(response){
    	var id = 'graph1';
    	var colors = graph1_colors;
    	var response_data = response.data;
    	populate_graph(colors, id, response_data);
    })
});

application.controller('relationshipChart', function($scope, $http){
	var url = hostname + '/relationship-statistics';

    $http.get(url)
    .then(function(response){
    	var id = 'graph2';
    	var colors = graph2_colors;
    	var response_data = response.data;
    	populate_graph(colors, id, response_data);
    })
});

application.controller('displayPersonDataCtrl', function($scope, $http){
	var url = hostname + '/filter-result';
	var person_list_url = hostname + '/filter-person?'
	var query = '';

    $http.get(url)
    .then(function(response){
    	var response_data = response.data;

        $scope.page_number = 1;
    	$scope.race = response_data['race'];
    	$scope.relationship = response_data['relationship'];
    	$scope.sex = response_data['sex'];
    	$scope.selected_sex = '';
    	$scope.selected_race = '';
    	$scope.selected_relationship = '';
    	$scope.query = '';

    })

    getPersonList(person_list_url, $http, $scope);

    $scope.nextPage = function(){
    	$scope.page_number += 1;
    	person_list_url = hostname + '/filter-person?' + $scope.query + 'page_number=' + $scope.page_number;
    	getPersonList(person_list_url, $http, $scope);
    }

    $scope.renderData = function(val, field) { 
        $scope.page_number = 1;

        if (field == 'sex') {
        	$scope.selected_sex = val;
        }
        if (field == 'race') {
            $scope.selected_race = val;
        }
        if (field == 'relationship') {
            $scope.selected_relationship = val;
        }

        var query = '';

        if ($scope.selected_sex.length > 0) {
        	query = query + 'sex=' + $scope.selected_sex + '&';
        }
        if ($scope.selected_race.length > 0) {
        	query = query + 'race=' +  $scope.selected_race + '&';
        }
        if ($scope.selected_relationship.length > 0) {
        	query = query + 'relationship=' +  $scope.selected_relationship + '&';
        }
        $scope.query = query;
        person_list_url = hostname + '/filter-person?' + query;
        getPersonList(person_list_url, $http, $scope);
    }
});

var getPersonList = function(url, $http, $scope) {
    var result = []
    $http.get(url)
    .then(function(response){
    	var response_data = response.data;
        $scope.person_list_data = response_data;
    }) 
}

var populate_graph = function(colors, id, response_data) {
    var ctx = document.getElementById(id).getContext('2d');
    var labels = []
    var data = []

	for(var i = 0; i < response_data.length; i++){
	    var item = response_data[i];

	    for (var field in item) {
	    	labels.push(field);
	    	data.push(item[field]);
	    }
	}

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: id,
                data: data,
                backgroundColor: colors,
                borderColor: colors,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}